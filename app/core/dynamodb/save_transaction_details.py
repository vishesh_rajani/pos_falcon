from app.core.utils.helpers import query_dynamo_db
from app.core.config import Config

def save_transaction_details(data):
	if 'mobile_number' in data:
		data.update({'mobile_no':int(data['mobile_number'])})
	query_dynamo_db(Config.DynamoDB.TABLE_TRANSACTION_DETAILS, 'INSERT', parameter=data)

