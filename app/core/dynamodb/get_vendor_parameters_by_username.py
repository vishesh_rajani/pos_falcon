from app.core.utils.helpers import query_dynamo_db
from app.core.config import Config

def get_vendor_parameters_by_username(data):
	results = query_dynamo_db(Config.DynamoDB.TABLE_VENDOR_DETAILS, 'QUERY-INDEX',
	                          condition={'username': str(data['username'])})
	return results