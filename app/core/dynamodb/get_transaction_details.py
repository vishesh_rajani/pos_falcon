from app.core.utils.helpers import query_dynamo_db
from app.core.config import Config

def get_transaction_details(data):
	condition = {'transaction_id': str(data['transaction_id']), 'mobile_no': int(data['mobile_number'])}
	response = query_dynamo_db(Config.DynamoDB.TABLE_TRANSACTION_DETAILS, 'QUERY', condition=condition)
	return response