from app.core.utils.helpers import query_dynamo_db
from app.core.config import Config

def get_vendor_parameters(data):
	condition = {'id': int(data['vendor_id'])}
	results = query_dynamo_db(Config.DynamoDB.TABLE_VENDOR_DETAILS, 'QUERY', condition=condition)
	return results