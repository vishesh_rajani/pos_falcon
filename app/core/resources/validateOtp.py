import falcon
from app.core.utils.helpers import *
import json
from datetime import datetime as dt
import os
from app.core.utils.validate_otp import validate_otp
from app.core.utils.wrapper_calls import call_payment_recipt, call_payment_status

class validateOtp(object):
    def on_post(self, req, resp):
        log = []
        data = req._params
        try:
            missing_key_response = mandatoryFields(['mobile_number', 'bill_amount', 'transaction_id', 'order_id', 'otp'], data)
            if missing_key_response:
                resp.body = json.dumps({'operation_status': '0', 'message': '', 'error_message': missing_key_response,
                                        'status': 'GN_E_001', })
                resp.status = falcon.HTTP_400
            else:
                log.append("trying validate otp")
                recipt_flag,max_retry,otp_expiry,status = validate_otp(data)
                log.append("recipt flag is "+str(recipt_flag)+" status is "+str(status))
                resp.status = falcon.HTTP_200
                if not recipt_flag:
                    if max_retry is None and otp_expiry is None:
                        resp.body = json.dumps({"operation_status": "0", "message": "Invalid Operation", "status": "VO_E_006"})
                    else:
                        resp.body = json.dumps({'operation_status': '0', 'max_retry': max_retry, 'otp_expiry': otp_expiry,
                                'otp_verification': 'False', 'status': status})
                else:
                    log.append("trying to hit payment recipt")
                    payment_status_flag,api_status_code,reason,status = call_payment_recipt(data)
                    print(payment_status_flag,api_status_code,status)
                    log.append("payment status flag is " + str(payment_status_flag) + " api status code is " + str(api_status_code)
                               +" status is "+str(status)+" reason is "+ str(reason))
                    if not payment_status_flag:
                        resp.status = falcon.HTTP_500
                        resp.body = json.dumps({'operation_status': '0','reason': reason,
                                                 'api': 'paymentReceipt', 'status': status,
                                                 'lms_response_status_code': api_status_code})
                    else:
                        log.append("trying to hit payment status")
                        flag,api_status_code,reason,status = call_payment_status(data)
                        log.append("flag is " + str(flag) + " reason is " + str(reason)+" status is "+str(status))
                        if flag:
                            resp.body = json.dumps({'operation_status': '1',
                                             'mobile_number': data['mobile_number'],
                                             'order_id': data['order_id'],
                                             'transaction_id': data['transaction_id'],
                                             'bill_amount': int(data['bill_amount']),
                                             'status': status})
                        else:
                            resp.status = falcon.HTTP_500
                            resp.body = json.dumps({'operation_status': '0',
                                               'api': 'paymentReceiptStatus',
                                               'lms_response_code': api_status_code,
                                                'status': status,
                                               'reason': reason})
        except Exception as e:
            log.append("under exception as" + str(e))
            resp.body = json.dumps({'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005'})
            resp.status = falcon.HTTP_500
        resp.context['log'] = log
        return
