import falcon
from app.core.utils.helpers import *
import json
from datetime import datetime as dt
import ast
import base64
from app.core.utils.revert_transaction import revert_transaction

class revertTransaction(object):
    def on_post(self, req, resp):
        log = []
        data = (req._params)
        print(data)
        try:
            missing_key_response = mandatoryFields(['mobile_number', 'bill_amount', 'transaction_id','order_id', 'original_transaction_id'], data)
            if missing_key_response:
                resp.body = json.dumps({'operation_status': '0', 'message': '', 'error_message': missing_key_response, 'status': 'GN_E_001', })
                resp.status = falcon.HTTP_400
            else:
                resp.status = falcon.HTTP_200
                log.append("about to hit revert transaction function")
                flag,status = revert_transaction(data)
                log.append("flag is "+str(flag)+" status is "+str(status))
                if flag:
                    resp.body = json.dumps({'operation_status': '1', 'message': '', 'status': status})
                else:
                    resp.body = json.dumps({'operation_status': '0', 'message': '', 'status': status})
        except Exception as e:
            log.append("error is "+str(e))
            resp.body = json.dumps({'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005'})
            resp.status = falcon.HTTP_500
        resp.context['log'] = log
        return
