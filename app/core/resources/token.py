import falcon
from app.core.utils.helpers import *
import json
import base64
from app.core.utils.jwt import create_token
from app.core.dynamodb.get_vendor_parameters_by_username import get_vendor_parameters_by_username
from app.core.utils.AESCypher import encrypt,decrypt
class token(object):
    def on_post(self, req, resp):
        data = (req._params)
        try:
            key_missing_response = mandatoryFields(['username', 'password'], data)
            if key_missing_response:
                resp.body = json.dumps(
                    {"operation_status": "0", "message": "", "error_message": key_missing_response, "status": "GN_E_001"})
                resp.status = 400
            else:
                results = get_vendor_parameters_by_username(data)
                if results['Success']:
                    results = results['Row']
                    try:
                        encrypt_pass = decrypt((Config.package_name).encode(),data['password']).decode()
                        print (encrypt_pass,results['password'])
                        if encrypt_pass != results['password']:
                            print("here")
                            resp.body = json.dumps({"operation_status": "0", "message": "", "status": "TN_E_001"})
                            resp.status = falcon.HTTP_403
                        else:
                            token = create_token(results['id'], results['name']).decode()
                            resp.body = json.dumps(
                                {"operation_status": "1", "token": token, "message": "", "status": "TN_S_001"})
                            resp.status = falcon.HTTP_200
                    except Exception as e:
                        resp.body = json.dumps({"operation_status": "0", "message": "", "status": "TN_E_001"})
                        resp.status = falcon.HTTP_403
                else:
                    resp.body = json.dumps({"operation_status": "0", "message": "", "status": "TN_E_001"})
                    resp.status = falcon.HTTP_403
        except Exception as e:
            resp.body = json.dumps(
                {'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005'})
            resp.status = falcon.HTTP_500
        return
