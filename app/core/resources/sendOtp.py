import falcon
from app.core.utils.helpers import *
from app.core.utils.send_otp import send_otp
import json
from datetime import datetime as dt
from datetime import datetime
import os

class sendOtp(object):
    def on_post(self, req, resp):
        log = []
        data = req._params
        try:
            missing_key_response = mandatoryFields(['mobile_number', 'bill_amount', 'transaction_id', 'order_id'], data)
            if missing_key_response:
                resp.body = json.dumps({'operation_status': '0', 'message': '', 'error_message': missing_key_response,
                                        'status': 'GN_E_001', })
                resp.status = falcon.HTTP_400
            else:
                log.append("about to hit send_otp function")
                otp_sent,max_retry,status = send_otp(data)
                log.append("send otp flag is "+str(otp_sent)+" max retry is "+str(max_retry)+" status is "+str(status))
                resp.status = falcon.HTTP_200
                body = {"mobile_number": data['mobile_number'], "otp_sent": otp_sent,
                                            "max_retry": max_retry, "message": "", "status": status}
                if not max_retry and otp_sent:
                    body.update({"operation_status": "1"})
                else:
                    body.update({"operation_status": "0"})
                resp.body = json.dumps(body)
        except Exception as e:
            log.append("error is "+str(e))
            resp.body = json.dumps({'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005'})
            resp.status = falcon.HTTP_500
        resp.context['log'] = log
        return
