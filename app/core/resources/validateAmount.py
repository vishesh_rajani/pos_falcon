import falcon
from app.core.utils.helpers import *
from app.core.utils.wrapper_calls import call_get_request_amount
import json
from app.core.dynamodb.get_vendor_parameters import get_vendor_parameters
from datetime import datetime as dt
import ast
import base64
class validateAmount(object):
    def on_post(self, req, resp):
        log = []
        scheme_id = ""
        data = (req._params)
        try:
            missing_key_response = mandatoryFields(['mobile_number', 'bill_amount', 'transaction_id', 'order_id'], data)
            if missing_key_response:
                resp.body = json.dumps({'operation_status': '0', 'message': '', 'error_message': missing_key_response, 'status': 'GN_E_001', })
                resp.status = falcon.HTTP_400
            else:
                if "scheme_id" in data:
                    scheme_id = data["scheme_id"]
                else:
                    log.append("trying to get scheme id from vendor parameters")
                    results = get_vendor_parameters(data)['Row']
                    scheme_id = ast.literal_eval(results['scheme_list'])[0]
                log.append("scheme id is = " + str(scheme_id))
                data.update({'scheme_id':scheme_id})
                log.append("about to hit get request amount wrapper")
                flag,api_status_code,status,customer_name,lan_no = call_get_request_amount(data)
                log.append("flag is "+str(flag)+" api status_code is "+str(api_status_code)+" status is "+str(status)+
                           " customer name is "+str(customer_name)+" lan no is "+str(lan_no))
                if flag:
                    resp.status = falcon.HTTP_200
                    if customer_name is not None and lan_no is not None:
                        print(data)
                        resp.body = json.dumps({'operation_status': '1', 'status': status, 'mobile_number': data["mobile_number"],
                         'customer_name': customer_name, 'customer_lan_number': lan_no})
                    else:
                        resp.body = json.dumps({"operation_status": "0", "message": "", "status": status})
                else:
                    resp.status = falcon.HTTP_500
                    resp.body = json.dumps({"operation_status": "0", "message": "", "api": "getRequestAmount",
                                            "lms_response_status_code": api_status_code, "status": status})
        except Exception as e:
            print (e)
            log.append("error is "+str(e))
            resp.body = json.dumps({'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005'})
            resp.status = falcon.HTTP_500
        resp.context['log'] = log
        return
