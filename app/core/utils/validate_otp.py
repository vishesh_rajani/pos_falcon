import falcon
from datetime import datetime as dt
from app.core.utils.helpers import query_dynamo_db, check_otp_validity
from app.core.config import Config
from app.core.dynamodb.get_transaction_details import get_transaction_details
from app.core.dynamodb.save_transaction_details import save_transaction_details

def validate_otp(data):
	status=None
	max_retry =None
	otp_expiry = None
	recipt_flag = False
	response = get_transaction_details(data)
	if response['Success']:
		response = response['Row']
		data.update({'scheme_id':response['scheme_id']})
		if response['bill_amount'] != data['bill_amount'] and response['order_id'] != data['order_id']:
			status = 'VO_E_006'
		else:
			response["otp_timestamp"] = dt.strptime(response['otp_timestamp'], Config.General.datetime_format)
			if 'no_of_attempts_verify' in response and response[
				"no_of_attempts_verify"] >= Config.SMS.no_of_attempts_verify:
				status = 'VO_E_001'
				max_retry = True
				otp_expiry = False
			elif response["otp"] == int(data['otp']):
				if not check_otp_validity(response["otp_timestamp"]) or (
						'verified' in response and response['verified'] == 1):
					status = 'VO_E_002'
					max_retry = False
					otp_expiry = True
				else:
					response['verified'] = 1
					recipt_flag = True
			else:
				response['no_of_attempts_verify'] = 0 if 'no_of_attempts_verify' not in response else response[
					'no_of_attempts_verify']
				response["no_of_attempts_verify"] = int(response["no_of_attempts_verify"]) + 1
				status = 'VO_E_003'
				max_retry= False
				otp_expiry = False
			response['otp_timestamp'] = response['otp_timestamp'].strftime(Config.General.datetime_format)
			save_transaction_details(response)
	else:
		status = 'VO_E_006'
	return recipt_flag,max_retry,otp_expiry,status