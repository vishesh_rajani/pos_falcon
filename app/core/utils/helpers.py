import base64
import re
import time
import traceback
import uuid
from datetime import datetime, date, timedelta
from random import randint
import boto3 as boto3
from boto3.dynamodb.conditions import Key
from app.aws_config import AWS
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto import Random
from decimal import Decimal
from app.core.config import Config, env
import requests
import json
import binascii

logClient = boto3.client('logs', region_name=AWS.REGION_NAME, aws_access_key_id=AWS.AWS_ACCESS_KEY_ID,
                         aws_secret_access_key=AWS.AWS_SECRET_ACCESS_KEY)
if env == "DEV":
    dynamoResource = boto3.resource('dynamodb', region_name=AWS.REGION_NAME, aws_access_key_id=AWS.AWS_ACCESS_KEY_ID,
                    aws_secret_access_key=AWS.AWS_SECRET_ACCESS_KEY)
else:
    dynamoResource = boto3.resource('dynamodb', region_name='ap-south-1')


def query_dynamo_db(table_name, operation, condition=None, parameter=None): # condition should in dict format
    try:
        table_obj = dynamoResource.Table(table_name)
        if operation == 'QUERY':
            # Query for select
            print ("Querying {table} on conditions {condition}".format(table=table_name, condition=condition))
            response = table_obj.get_item(Key=condition)
            print( response)
            response = response['Item']
        elif operation == 'UPDATE' or operation == 'INSERT':
            # Query for insert or update
            print ("Puting object {parameter} in {table}".format(table=table_name, parameter=parameter))
            response = table_obj.put_item(Item=parameter)
        elif operation == 'QUERY-INDEX':
            print ("Querying {table} on conditions {condition}".format(table=table_name, condition=condition))
            print(list(condition.keys())[0])
            index = str(list(condition.keys())[0])
            index_name = index+"-index"
            response = table_obj.query(IndexName=index_name,KeyConditionExpression=Key(index).eq(condition[index]))
            print (response)
            response = response['Items'][0]
        Success = True
        Row = response
    except Exception as e:
        print ("query_dynamo_db error: " + str(e) + " table: " + table_name + " operation: " + operation + " condition: " + str(condition) + " parameter:" + str(parameter))
        Success = False
        Row = ""
    return {'Success': Success, 'Row': Row}

def get_status(resp):
    try:
        results = query_dynamo_db('abfl_pos_status_code', 'QUERY', condition={'status_code': str(resp['status'])})['Row']
        resp['message'] = str(results['status'])
    except Exception as e:
        print (e)
    return resp



def sms_otp(mobile_number, transaction_id, bill_amount, otp=None,ref_type=None):
    if otp is not None:
        requestData = {
            'message': "{} is Your OTP for amount {} with Transaction Id {}".format(otp, bill_amount, transaction_id),
            'sender': Config.SMS.sender_id, 'sms': {}}
    else:
        if ref_type == "D":
            requestData = {
                'message': "An amount of Rs. {} has been debited from your account with transaction id {}."
                    .format(bill_amount,transaction_id),
                'sender': Config.SMS.sender_id, 'sms': {}}
        else:
            requestData = {
                'message': "An amount of Rs. {} will be credited to your account with transaction id {} within 2-5 business working days."
                    .format(bill_amount,transaction_id),
                'sender': Config.SMS.sender_id, 'sms': {}}

    requestData['sms']['to'] = mobile_number
    requestData['sms']['msgid'] = "1"
    count = 0
    while count < 3:
        count += 1
        response = requests.get(
            Config.SMS.url + Config.SMS.api_key + "&method=sms.json&json=" + json.dumps(requestData))
        response = json.loads(response.text)
        if response['status'] == 'OK':
            return True
    return False

def generate_random_number_with_n_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


def check_otp_validity(timestamp):
    now = datetime.now()
    difference = now - timestamp
    duration_in_s = difference.total_seconds()
    print (divmod(duration_in_s, 60)[0])
    if int(divmod(duration_in_s, 60)[0]) >= Config.SMS.otp_validity_mins:
        return False
    else:
        return True

def create_request_from_db(request_data, data):
    if isinstance(request_data, str):
        with open("app/core/Request_json/" + request_data + ".json") as json_file:
            request_data = json.load(json_file)
    temp = ""
    for key, db_field in request_data.items():
        print (key, db_field)
        try:
            if isinstance(db_field, dict):
                print ("Here")
                request_data[key] = create_request_from_db(db_field, data)
                print ('key', request_data[key])
            elif isinstance(db_field, list):
                for i in db_field:
                    if isinstance(i, dict):
                        print ("Here")
                        request_data[key][i] = create_request_from_db(i, data)
                    else:
                        temp += data[i] + " "
                        request_data[key] = temp
            else:
                request_data[key] = data[db_field]
        except Exception as e:
            print( "request error", e)
            pass
    return request_data


def cloud_watch(vendor_name, scheme_id, api_name, transaction_id, message):
    vendor_name = vendor_name.replace(" ", "_")
    # s3_message=str(datetime.now().strftime("%H:%m"))+"\t"+"Log Starts "
    log_stream_name = str(transaction_id) + '_' + str(scheme_id) + '_' + str(api_name) + '_' + str(uuid.uuid4())
    print (log_stream_name)
    print (message)
    try:

        stream_response = logClient.describe_log_streams(

            logGroupName='POS' + '_' + vendor_name,
            logStreamNamePrefix=log_stream_name
        )
    except:
        try:
            print ("inside create")
            response_create = logClient.create_log_group(logGroupName='POS' + '_' + vendor_name)
        except:
            print ('POS' + '_' + vendor_name, '=>Already Exist log group')
        stream_response = logClient.describe_log_streams(

            logGroupName='POS' + '_' + vendor_name,
            logStreamNamePrefix=log_stream_name
        )

    if len(stream_response['logStreams']) == 0:
        create_stream_response = logClient.create_log_stream(logGroupName='POS' + '_' + vendor_name,
                                                             logStreamName=log_stream_name)
        print (create_stream_response)

    put_log_response = logClient.put_log_events(
        logGroupName='POS' + '_' + vendor_name,
        logStreamName=log_stream_name,
        logEvents=[
            {
                'timestamp': int(time.time()) * 1000,
                'message': message
            }
        ]
    )
    return 'Log added successfully'


def mandatoryFields(fields, row):
    error_message = ""
    for field in fields:
        if field not in row:
            return "{} key is missing".format(field)
        elif row[field] == '':
            return "{} key is empty".format(field)
    return error_message

def default(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    if isinstance(obj, datetime):
        return str(obj)
    if isinstance(obj, date):
        return str(obj)
    raise TypeError("Object of type '%s' is not JSON serializable" %
                    type(obj).__name__)

def get_month_and_year_due_date(months, day, date):
    date = datetime.strptime(date, "%Y-%m-%d")
    if date.month == 12 and months!=0:
        month = str(months)
        year = str(date.year + 1)
    elif date.month + months > 12:
        month = str((date.month + months) % 12)
        year = str(date.year + 1)
    else:
        month = str(date.month + months)
        year = str(date.year)
    return str(year + "-" + month + "-" + str(day))

def get_wrapper_token():
    url = Config.WRAPPER.base_url + 'login'
    print (url)
    payload = {
        'username': Config.WRAPPER.username,
        'password': Config.WRAPPER.password
    }
    print (payload)
    response = requests.post(url=url,data=payload)
    result = json.loads(response.text)
    return result['Token']

def api_call(url,request_data,headers):
    url_mapping = {
        'get_request_amount':Config.WRAPPER.base_url + Config.WRAPPER.version + Config.WRAPPER.get_request_amount_api,
        'payment_recipt':Config.WRAPPER.base_url + Config.WRAPPER.version + Config.WRAPPER.payment_recipt_api,
        'payment_status':Config.WRAPPER.base_url + Config.WRAPPER.version + Config.WRAPPER.payment_status_api
    }
    r = requests.post(
        url=url_mapping[url],
        data=json.dumps(request_data, default=default), headers=headers)
    return json.loads(r.content)



