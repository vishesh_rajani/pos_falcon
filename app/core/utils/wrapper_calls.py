import falcon
from app.core.utils.helpers import *
import json
import base64
import os
import requests
from app.core.config import Config
from app.core.dynamodb.get_transaction_details import get_transaction_details
from app.core.dynamodb.save_transaction_details import save_transaction_details
from app.core.dynamodb.get_vendor_parameters import get_vendor_parameters
from datetime import datetime as dt

def call_get_request_amount(data):
	request_data = create_request_from_db("pos_get_request_amount", data)
	flag=True
	status = None
	api_status_code= None
	response = {}
	results = get_transaction_details(data)
	if results['Success']:
		status = 'VA_E_003'
	else:
		lms_wrapper_token = get_wrapper_token()
		headers = {'auth': lms_wrapper_token}
		r = api_call('get_request_amount',request_data,headers)
		if r['status_code'] != 200:
			flag=False
			status = "GN_E_006"
			api_status_code = r['status_code']
		else:
			response = json.loads(r['content'])
			if response['servicableFlag'] == "YES":
				parameter = {'transaction_id': str(data['transaction_id']), 'order_id': str(data['order_id']),
				             'mobile_no': int(data['mobile_number']), 'vendor_name': str(data['vendor_name']),
				             'scheme_id': data['scheme_id'], 'bill_amount': data['bill_amount'],
				             'lan_no': str(response['loanNumber']),
				             'timestamp': str(dt.now().strftime(Config.General.datetime_format)), 'deal_no': response['dealNumber'],
				             'ref_type': 'D'}
				save_transaction_details(parameter)
				status = 'VA_S_001'
			elif response['servicableFlag'] is None:
				if response['operation_message'] == "mobilenoisinvalid":
					status='VA_E_002'
				else:
					status = 'VA_E_001'
	return flag,api_status_code,status,response.get('customerName'),response.get('loanNumber')

def call_payment_recipt(data):
	payment_status_flag = False
	status = None
	api_status_code = None
	reason =None
	response = get_transaction_details(data)['Row']
	if 'ref_type' in response and response['ref_type'] == "D":
		results = get_vendor_parameters(data)['Row']
		vendor_duedate = results['due_date']
		curr_date = dt.now()
		if str(curr_date.day) < vendor_duedate:
			duedate = get_month_and_year_due_date(1, vendor_duedate, curr_date.strftime("%Y-%m-%d"))
		else:
			duedate = get_month_and_year_due_date(2, vendor_duedate, curr_date.strftime("%Y-%m-%d"))
		response['due_date'] = duedate
	request_data = create_request_from_db('pos_payment_recipt', response)
	lms_wrapper_token = get_wrapper_token()
	headers = {'auth': lms_wrapper_token}
	r = api_call('payment_recipt',request_data,headers)
	print(r)
	api_status_code = r['status_code']
	if r['status_code'] == 200:
		content = json.loads(r['content'])
		print(content)
		if content['operationStatus'] == "1":
			if content['paymentReceiptApiResponse'] is None:
				status = 'GN_E_006'
				reason = str(content['failureReason'])
			else:
				response['payment_receipt_ref_no'] = content['paymentReceiptApiResponse'][0]['paymentReceiptApiReferenceNo']
				save_transaction_details(response)
				payment_status_flag = True
		else:
			status = 'GN_E_006'
	else:
		status = 'GN_E_006'
	print(status,payment_status_flag,reason)
	return payment_status_flag,api_status_code,reason,status

def call_payment_status(data,check_transaction_flag=None):
	flag =False
	status =None
	reason =None
	api_status_code = None
	response = get_transaction_details(data)['Row']
	request_data = create_request_from_db('pos_payment_status', response)
	lms_wrapper_token = get_wrapper_token()
	headers = {'auth': lms_wrapper_token}
	r = api_call('payment_status',request_data,headers)
	api_status_code = r['status_code']
	if r['status_code'] == 200:
		content = json.loads(r['content'])
		print(content)
		if content['operationStatus'] != "1":
			status = 'GN_E_006'
		else:
			if content['crPmtReceiptRefResDtl'][0]['status'] == 'E':
				status = 'GN_E_006'
				reason = str(content['crPmtReceiptRefResDtl'][0]['process_ACTION'])
			else:
				if os.getenv('Testing', 'DEV') == "DEV" and check_transaction_flag is None:
					sms_otp(data['mobile_number'], data['transaction_id'],
					          response['bill_amount'], response['ref_type'])
				response['status'] = True
				response['timestamp'] = str(dt.now().strftime(Config.General.datetime_format))
				save_transaction_details(response)
				status = 'VO_S_001' if check_transaction_flag is None else 'CT_S_001'
				if response['ref_type'] == 'R':
					if check_transaction_flag:
						status = 'CT_S_002'
					else:
						status = 'VO_S_002'
						condition = {
							'transaction_id': str(response['original_transaction_id']),
							'mobile_number': int(data['mobile_number'])
						}
						original_response = get_transaction_details(condition)['Row']
						original_response['reversed'] = True
						original_response['reversed_timestamp'] = str(dt.now().strftime(Config.General.datetime_format))
						save_transaction_details(original_response)
				flag =True
	else:
		status='GN_E_006'
	return flag,api_status_code,reason,status