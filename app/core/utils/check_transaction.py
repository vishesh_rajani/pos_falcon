import falcon
from datetime import datetime as dt
from app.core.utils.helpers import query_dynamo_db,create_request_from_db, api_call
from app.core.config import Config
from app.core.dynamodb.save_transaction_details import save_transaction_details
from app.core.dynamodb.get_transaction_details import get_transaction_details
from app.core.utils.wrapper_calls import call_payment_status

def check_transaction(data):
	status = None
	lms_fail_flag = False
	flag = False
	reason = None
	api_status_code = None
	results = get_transaction_details(data)
	if results['Success']:
		results = results['Row']
		data.update({'scheme_id':results['scheme_id']})
		if int(results['bill_amount']) == int(data['bill_amount']) and results['order_id'] == data['order_id']:
			if 'payment_receipt_ref_no' in results and len(results['payment_receipt_ref_no']) !=0:
				flag, api_status_code, reason, status = call_payment_status(data,True)
				if not flag:
					lms_fail_flag =True
			else:
				status = 'CT_E_001'
				if 'reversed' in results and results['reversed']:
					status = 'CT_E_002'
	else:
		status = 'CT_E_002'
	print(api_status_code,reason)
	return flag,lms_fail_flag,api_status_code,reason,status