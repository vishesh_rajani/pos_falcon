import base64
import calendar
from functools import wraps
from datetime import datetime as dt
import datetime
import jwt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet
from ..config import Config

password_provided = "thisisencryptionforjwttoken"  # This is input in the form of a string
password = password_provided.encode()  # Convert to type bytes
salt = b'\xf1?\xee\xa5)\xb9B\xfd\xef\tBo~g\x99I'
kdf = PBKDF2HMAC(
    algorithm=hashes.SHA256(),
    length=32,
    salt=salt,
    iterations=100000,
    backend=default_backend()
)

key = base64.urlsafe_b64encode(kdf.derive(password))

f = Fernet(key)


def encrypt(message):
    return f.encrypt(message)


def decrypt(message):
    return f.decrypt(message)

def create_token(vendor_id, vendor_name):
    print ("inside create token")
    payload = {
        'vendor_id': str(encrypt(str(vendor_id).encode())),
        'vendor_name': str(encrypt(str(vendor_name).encode())),
        'iat': calendar.timegm(dt.utcnow().timetuple()),
        'exp': calendar.timegm((dt.utcnow() + datetime.timedelta(hours=(24 * 7))).timetuple()),
        'iss': 'abfl-jwt',
    }
    print(payload)
    token = jwt.encode(payload, Config.SECRET_KEY, algorithm='HS256')
    return token