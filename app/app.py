import falcon
from .middleware.auth_midleware import AuthMiddleware
from falcon_multipart.middleware import MultipartMiddleware
from .middleware.cloudwatch_middleware import Cloudwatch
from .middleware.populate_status import PopulateStatus

api = falcon.API(middleware=[MultipartMiddleware(),
                             AuthMiddleware(),PopulateStatus(),Cloudwatch()])
api.req_options.auto_parse_form_urlencoded = True

import app.routes