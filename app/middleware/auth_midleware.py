import json

import falcon
from app.core.config import Config
from app.core.utils.jwt import create_token
import base64
import calendar
from functools import wraps
from datetime import datetime as dt
import datetime
import jwt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet

password_provided = "thisisencryptionforjwttoken"  # This is input in the form of a string
password = password_provided.encode()  # Convert to type bytes
salt = b'\xf1?\xee\xa5)\xb9B\xfd\xef\tBo~g\x99I'
kdf = PBKDF2HMAC(
	algorithm=hashes.SHA256(),
	length=32,
	salt=salt,
	iterations=100000,
	backend=default_backend()
)

key = base64.urlsafe_b64encode(kdf.derive(password))

f = Fernet(key)


def encrypt(message):
	return f.encrypt(message)


def decrypt(message):
	return f.decrypt(message)


class AuthMiddleware(object):
	def process_request(self, req, resp):
		try:
			if not req.headers.get('TOKEN'):
				resp.body = json.dumps({'operation_status': '0', 'status': 'missing token header', 'status_code': '3'})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			try:
				token = req.headers.get('TOKEN')
				if token:
					vendor_details = jwt.decode(token, Config.SECRET_KEY, algorithms='HS256')
					print(vendor_details)
					vendor_id = decrypt(vendor_details['vendor_id'].encode())
					vendor_id = vendor_id.decode()
					vendor_name = decrypt(vendor_details['vendor_name'].encode())
					vendor_name = vendor_name.decode()
					req._params.update({'vendor_id':vendor_id,'vendor_name':vendor_name})
				else:
					resp.body = json.dumps({'msg': 'Error in decoding token'})
					resp.complete = True
					return
			except jwt.DecodeError:
				resp.body = json.dumps({'operation_status': '0', 'message': 'token is invalid', 'status': 'GN_E_003'})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except jwt.ExpiredSignature:
				resp.body = json.dumps({'operation_status': '0', 'message': 'token has expired', 'status': 'GN_E_004'})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except jwt.InvalidIssuedAtError:
				resp.body = json.dumps({'operation_status': '0', 'message': 'Issued at time doesnt look right', 'status': ''})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except jwt.InvalidIssuerError:
				resp.body = json.dumps({'operation_status': '0', 'message': 'Doesnt look like we issued this token',
				             'status': ''})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
		except:
			pass
