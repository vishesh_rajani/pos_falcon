import falcon
from app.core.utils.helpers import cloud_watch
import json

class Cloudwatch(object):
	def process_response(self, req, resp, resource, req_succeeded):
		try:
			data = req._params
			scheme_id = data['scheme_id'] if 'scheme_id' in data else ""
			if 'transaction_id' in data:
				message = 'request_body => ' + str(data) + '\n\n' + 'response_body => ' + str(
					resp) + '\n\n' + 'logs => ' + str(resp.context['log'])
				cloud_watch(vendor_name=data['vendor_name'], scheme_id=scheme_id, api_name=req.path,
				            transaction_id=data['transaction_id'],
				            message=message)
		except:
			pass