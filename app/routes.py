from app.app import api
from app.core.resources.health import health
from app.core.resources.token import token
from app.core.resources.validateAmount import validateAmount
from app.core.resources.sendOtp import sendOtp
from app.core.resources.validateOtp import validateOtp
from app.core.resources.revertTransaction import revertTransaction
from app.core.resources.checkTransaction import checkTransaction

api.add_route('/health', health())
api.add_route('/token',token())
api.add_route('/validateAmount',validateAmount())
api.add_route('/sendOtp',sendOtp())
api.add_route('/validateOtp',validateOtp())
api.add_route('/revertTransaction',revertTransaction())
api.add_route('/checkTransaction',checkTransaction())