from wsgiref import simple_server

from app.app import api


def start_server():
    server = simple_server.make_server('0.0.0.0', 9000, api)
    server.serve_forever()


if __name__ == "__main__":
    start_server()
